import { Config } from "@stencil/core";

export const config: Config = {
  namespace: "userprofile",
  outputTargets: [
    {
      type: "dist"
    },
    {
      type: "www",
      serviceWorker: null
    }
  ],
  globalStyle: "src/global/variables.css",
  globalScript: "src/global.ts"
};
