import { newE2EPage } from "@stencil/core/testing";

describe("user-picture", () => {
  it("renders", async () => {
    const page = await newE2EPage();
    await page.setContent(`<user-picture url="abc"></user-picture>`);
    const element = await page.find("user-picture");
    expect(element).toHaveClass("hydrated");
  });
  it("renders changes to the desc", async () => {
    const page = await newE2EPage();
    await page.setContent(
      `<user-picture url="abc" desc="Paris"></user-picture>`
    );
    const element = await page.find("user-picture >>> figcaption");
    expect(element.textContent).toEqual(`Paris`);
  });
});
