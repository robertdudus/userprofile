import { Component, Prop, ComponentInterface } from "@stencil/core";

@Component({
  tag: "user-picture",
  styleUrl: "user-picture.css",
  styleUrls: {
    ios: "user-picture.ios.css"
    // wp: 'user-picture.wp.css',
    // md: 'user-picture.md.css',
    // desktop: 'user-picture.css',
  },
  // host: {
  //   theme: "something"
  // },
  shadow: true
})
export class UserPicture implements ComponentInterface {
  @Prop()
  url!: string;
  @Prop()
  desc?: string;

  render() {
    return (
      <figure>
        <img src={this.url} />
        {this.desc && <figcaption>{this.desc}</figcaption>}
      </figure>
    );
  }
}
