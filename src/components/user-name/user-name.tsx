import { Component, Prop } from "@stencil/core";

@Component({
  tag: "user-name",
  styleUrl: "user-name.css",
  shadow: true
})
export class UserName {
  @Prop()
  first!: string;
  @Prop()
  middle?: string;
  @Prop()
  last?: string;
  @Prop()
  intention: string = "long form";

  render() {
    return (this.intention === "short form") ? 
    (
      <div class="short">
        Name: {this.first} {this.middle && (<span>{this.middle.substr(0,1)}</span>)} {this.last && (<span>{this.last}</span>)}
      </div>
    ) :
    (
      <div>
        <div>
          <span>V0.1.2 First Name:</span>
          <span id="first">{this.first}</span>
        </div>
        {this.middle && (
          <div>
            <span>Middle Name:</span>
            <span id="middle">{this.middle}</span>
          </div>
        )}
        {this.last && (
          <div>
            <span>Last Name:</span>
            <span id="last">{this.last}</span>
          </div>
        )}
      </div>
    );
  }
}
