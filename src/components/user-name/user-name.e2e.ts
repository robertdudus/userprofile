import { newE2EPage } from "@stencil/core/testing";

describe("user-name", () => {
  it("renders", async () => {
    const page = await newE2EPage();

    await page.setContent(`<user-name first="abc"></user-name>`);
    const element = await page.find("user-name");
    expect(element).toHaveClass("hydrated");
  });

  it("renders changes to the first, middle and the last name", async () => {
    const page = await newE2EPage();

    await page.setContent(
      `<user-name first="a" middle="b" last="c"></user-name>`
    );
    let element = await page.find("user-name >>> #first");
    expect(element.textContent).toEqual(`a`);

    element = await page.find("user-name >>> #middle");
    expect(element.textContent).toEqual(`b`);

    element = await page.find("user-name >>> #last");
    expect(element.textContent).toEqual(`c`);
  });
});
