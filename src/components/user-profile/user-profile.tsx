import { Component, Prop, ComponentInterface } from "@stencil/core";

@Component({
  tag: "user-profile",
  styleUrl: "user-profile.css",
  shadow: true
})
export class UserProfile implements ComponentInterface {
  @Prop()
  apiUser!: string;
  @Prop()
  userId!: string;
  @Prop()
  intention: string = "long profile";

  private userPictureData: { url: string; desc: string };
  private userNameData: { first: string; middle: string; last: string };

  componentWillLoad() {
    // use fetch(`${this.apiUser}/getUserPictureData/${this.userId}`) to get user's picture data
    this.userPictureData = {
      url: `https://dummyimage.com/400x300/6e6c24/fff.jpg&text=User+${
        this.userId
      }+Image`,
      desc: `Desc for user ${this.userId}`
    };
    // use fetch(`${this.apiUser}/getUserNameData/${this.userId}`) to get user's name data
    this.userNameData = {
      first: this.intention === "short profile" ? `F:${this.userId}` : `FN is ${this.userId}`,
      middle: this.intention === "short profile" ? `${this.userId}` : `MN is ${this.userId}`,
      last: this.intention === "short profile" ? `L:${this.userId}` : `LN is ${this.userId}`
    };
  }

  render() {
    return (
      <div>
        <div>User Profile V1.0.0</div>
        <user-picture
          url={this.userPictureData.url}
          desc={this.userPictureData.desc}
        />
        {this.intention === "short profile" ? 
        (<user-name
          first={this.userNameData.first}
          middle={this.userNameData.middle}
          last={this.userNameData.last}
          intention="short form"
        />) :
        (<user-name
          first={this.userNameData.first}
          middle={this.userNameData.middle}
          last={this.userNameData.last}
        />)}
      </div>
    );
  }
}
